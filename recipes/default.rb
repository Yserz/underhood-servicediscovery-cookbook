#
# Cookbook Name:: underhood-service-discovery
# Recipe:: default
#
# Copyright 2014, Michael Koppen
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
include_recipe 'java'

Chef::Log.info "Creating UnderHood group #{node['underhood']['group']}"
group node['underhood']['group']

Chef::Log.info "Creating UnderHood user #{node['underhood']['user']}"
user node['underhood']['user'] do
  comment 'UnderHood Service Discovery'
  group node['underhood']['group']
  #home node['underhood']['base_dir']
  shell '/bin/bash'
  system true
end

Chef::Log.info "Creating UnderHood base directory #{node['underhood']['base_dir']}"
directory "#{node['underhood']['base_dir']}" do
  owner node['underhood']['user']
  group node['underhood']['group']
  action :create
  mode '0755'
  recursive true
end

Chef::Log.info "Creating UnderHood config directory #{node['underhood']['base_dir']}/config"
directory "#{node['underhood']['base_dir']}/config" do
  owner node['underhood']['user']
  group node['underhood']['group']
  action :create
  mode '0755'
  recursive true
end

Chef::Log.info "Creating UnderHood logging directory #{node['underhood']['base_dir']}/log"
directory "#{node['underhood']['base_dir']}/log" do
  owner node['underhood']['user']
  group node['underhood']['group']
  action :create
  mode '0755'
  recursive true
end

Chef::Log.info "Downloading UnderHood JAR from #{node['underhood']['package_url']} to #{node['underhood']['base_dir']}/UnderHood-full.jar"
remote_file "#{node['underhood']['base_dir']}/UnderHood-full.jar" do
  source node['underhood']['package_url']
  owner node['underhood']['user']
  group node['underhood']['group']
  mode '0700'
  action :create
  notifies :restart, 'service[underhood-sd]'
end

Chef::Log.info "Creating UnderHood launcher config #{node['underhood']['base_dir']}/config"
template "#{node['underhood']['base_dir']}/config/launcher.properties" do
  source 'launcher.properties.erb'
  owner node['underhood']['user']
  group node['underhood']['group']
  mode 0644
  action :create
  notifies :restart, 'service[underhood-sd]'
end

Chef::Log.info "Creating UnderHood underhood config #{node['underhood']['base_dir']}/config"
template "#{node['underhood']['base_dir']}/config/underhood.properties" do
  source 'underhood.properties.erb'
  owner node['underhood']['user']
  group node['underhood']['group']
  mode 0644
  action :create
  notifies :restart, 'service[underhood-sd]'
end

Chef::Log.info "Creating UnderHood daemon script in /etc/init.d/"
template "/etc/init.d/underhood-sd" do
  source 'underhood-sd.erb'
  mode 0755
  action :create
end

service "underhood-sd" do
  #QUICKFIX: normal action should be 'action :restart' but due to a bug in service implementation
  #Chef does not correctly determine if the service is running or not.
  action :restart
  supports :status => true, :start => true, :stop => true, :restart => true
end
