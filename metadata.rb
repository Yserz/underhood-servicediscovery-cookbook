name             'underhood-service-discovery'
maintainer       'Michael Koppen'
maintainer_email 'koppen@fh-brandenburg.de'
license          'Apache 2.0'
description      'Installs/Configures underhood-service-discovery'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'


supports 'ubuntu'

depends 'java'