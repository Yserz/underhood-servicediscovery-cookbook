#
# Copyright Michael Koppen
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#<> UnderHood User: The user that UnderHood executes as.
default['underhood']['user'] = 'underhood'
#<> UnderHood Admin Group: The group allowed to manage UnderHood.
default['underhood']['group'] = 'underhood-admin'

#<> Version: The version of the UnderHood install package.
#default['underhood']['version'] = "3.1.2.2"
#<> Package URL: The url to the UnderHood install package.
default['underhood']['package_url'] = "https://bitbucket.org/Yserz/underhood-servicediscovery-cookbook/downloads/UnderHood-full.jar"
#<> UnderHood Base Directory: The base directory of the UnderHood install.
default['underhood']['base_dir'] = '/usr/local/underhood'

#<> Discovery-Service-Name: The name of the service which will be populated through Service-Discovery
default['underhood']['discovery']['service']['name'] = "UnderHood"
#<> Discovery-Service-Type: The type of the service which will be populated through Service-Discovery
default['underhood']['discovery']['service']['type'] = "_underhood"
#<> Discovery-Service-Port: The port of the service which will be populated through Service-Discovery
default['underhood']['discovery']['service']['port'] = "0"